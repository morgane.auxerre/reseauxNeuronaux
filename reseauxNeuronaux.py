# Lien gitLab : https://gitlab.com/morgane.auxerre/reseauxNeuronaux/

import tensorflow as tf
import numpy as np
import math, random
import matplotlib.pyplot as plt

def choixNeurone():
    n = input('Choisir entre 1 -> Neurone formel et 2 -> TensorFlow : ')
    if n not in [1,2,3,4]:
        return choixNeurone()
    return n

def ChooseAFunction1():
    GoOn=0
    ChoosenFunction=input('Choisir entre 1 -> Affine et 2 -> exponentielle : ')
    if ChoosenFunction not in [1,2]:
        return ChooseAFunction1()
    return ChoosenFunction

def ChooseAFunction2():
    GoOn=0
    ChoosenFunction=input('Choisir entre 1 -> Affine, 2 -> exponentielle, 3 -> Hyperbole et 4 -> triggo : ')
    if ChoosenFunction not in [1,2,3,4]:
        return ChooseAFunction2()
    return ChoosenFunction

def formelAffine():
    ainit=random.randint(1,10)
    a=ainit
    binit=random.randint(1,10)
    b=binit
    pr=1
    x,y,yapp=[],[],[]
    iteration=1000
    secreta=random.randint(-10,10)
    secretb=random.randint(-10,10)
    print("Starting with a= {0} and b= {1}".format(secreta,secretb))
    for lex in range(iteration):
        x.append(lex)
        y.append(lex*secreta+secretb)
    ##Le pas est la modification entre chaque iteration
    pas=0.1
    pasa=pasb=pas
    #Facteur de tolerance
    tolerance=70
    ##Difference entre la valeur et la valeur recherchee
    diff=0
    ##ancienne difference
    pdiff=None
    pdiffb=None
    diffs,iterates=[],[]
    print("For y=ax+b, with a= '{0}' and b='{1}' and optimizer='{2}' with '{3}' iterations Begining with :a&b:'{4}' &'{5}'".format(secreta,secretb,pas,iteration,ainit,binit))
    #For the loop, are already used : i, j, k, l
    # In each loop, if the previous result is more closer than the new one, we change the sign ! and so one.
    for j in range(len(x)):
        iterates.append(j)
        diffb=pdiff
        res=a*x[j]+b
        yapp.append(res)
        diff=abs(round(res-y[j],pr))
        diffs.append(diff)
        if diff==0:
            continue
        if pdiff!=None:
            if pdiff<diff:
                pasa=-pasa
        diff=abs(round(res-y[j],pr))
        if(diff>tolerance):
            a=round(a+2*pasa,pr)
        if pdiff!=None:
            if pdiff<diff:
                pasb=-pasb
        b=round(b+pasb,pr)
        pdiff=diff
    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax1.plot(iterates, diffs)
    ax1.set_title("Evolution de la difference")
    ax3 = fig.add_subplot(212)
    ax3.plot(x, y,'r-',x,yapp,'-')
    ax3.set_title("courbe originale")
    plt.show()


def formelExpo():
    #Parameters :: role should'nt be touch,  pr is precision (round), pas is the step by what you increase your guessed number, iteration... is obvious
    role=0
    pr=15
    pas=0.1
    iteration=1000
    a=2.0
    #End of Parameters
    #Getting some data to learn ::
    x,y,yapp,iterates,diffs=[],[],[],[],[]
    diff=None
    pdiff=None
    for lx in range(iteration):
        lex=lx+3
        x.append(lex/10)
        y.append(math.exp(lex/10))
    #And then learn. with 1 neuron...
    for i in range(len(x)):
        iterates.append(x[i])
        res=a**x[i]
        yapp.append(res)
        diff=abs(round(res-y[i],pr))
        diffs.append(diff)
        if diff==0:
            continue
        if pdiff!=None:
            if pdiff*2<diff:
                role+=1
                pas=-pas
                if role>10:
                    pas=(pas/10)
                    role=0
        a=round(a+pas,pr)
        pdiff=diff
    print("value expected :'{0}'".format(a))
    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax1.plot(x, diffs)
    ax1.set_title("evolution de la difference")
    ax3 = fig.add_subplot(212)
    ax3.plot(x, y,'r-',x,yapp,'g--')
    ax3.set_title("courbes")
    plt.show()


def tensor2(selection):
    ##Handle the weight
    def poids(shape, init_method='charles', charles_params = (None, None)):
        if init_method == 'zeros':
            return tf.Variable(tf.zeros(shape, dtype=tf.float32))
        elif init_method == 'uniform':
            return tf.Variable(tf.random_normal(shape, stddev=0.01, dtype=tf.float32))
        else:
            (fan_in, fan_out) = charles_params
            low = -4*np.sqrt(6.0/(fan_in + fan_out))
            high = 4*np.sqrt(6.0/(fan_in + fan_out))
            return tf.Variable(tf.random_uniform(shape, minval=low, maxval=high, dtype=tf.float32))
    ##Model of learning
    def model(X, num_hidden=10):
        w_h = poids([1, num_hidden], 'charles', charles_params=(1, num_hidden))
        b_h = poids([1, num_hidden], 'zeros')
        h = tf.nn.sigmoid(tf.matmul(X, w_h) + b_h)
        w_o = poids([num_hidden, 1], 'charles', charles_params=(num_hidden, 1))
        b_o = poids([1, 1], 'zeros')
        return tf.matmul(h, w_o) + b_o
    iterationLearn=int(input("Nombre d'iterations (au moins 1000) : "))
    np.random.seed(1000) # for repro
    if selection==1:
        myModel = lambda x: 7*x+5
    elif selection==2:
        myModel = lambda x: np.exp(x)/np.exp(x-1)
    elif selection==3:
        myModel = lambda x: np.cos(x)+3
    else:
        myModel = lambda x: x**2
    hidenode,num,MINB = 20,1000,100
    all_x = np.float32( np.random.uniform(-10, 10, (1, num))).T
    lengtrain = int(num*0.7)
    x_for_train,x_check= all_x[:lengtrain],all_x[lengtrain:]
    y_for_train,y_check=myModel(x_for_train),myModel(x_check)
    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax1.scatter(x_for_train, y_for_train, c='blue',s=1, label='training')
    ax1.scatter(x_check, y_check, c='red',s=1, label='validate')
    X = tf.placeholder(tf.float32, [None, 1], name="X")
    Y = tf.placeholder(tf.float32, [None, 1], name="Y")
    resu = model(X, hidenode)
    train_op = tf.train.AdamOptimizer().minimize(tf.nn.l2_loss(resu - Y))
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    errors,test,firstBlood=[],[],[]
    for i in range(iterationLearn):
        #Entrainement
        for start, end in zip(range(0, len(x_for_train), MINB), range(MINB, len(x_for_train), MINB)):
            sess.run(train_op, feed_dict={X: x_for_train[start:end], Y: y_for_train[start:end]})
            #resultat
        deltas = sess.run(tf.nn.l2_loss(resu - y_check),  feed_dict={X:x_check})
        errors.append(deltas)
        chiffre=sess.run(resu,feed_dict={X:x_check})
        if i==0:
            firstBlood=chiffre
        elif i==iterationLearn-1:
            test=chiffre
        if i%100 == 0: print ("iteration %d,  deltas %g" % (i, deltas))
    ax1.scatter(x_check,firstBlood, c='violet',s=1, label='validate')
    ax1.scatter(x_check,test, c='orange',s=1, label='validate')
    ax2=fig.add_subplot(212)
    ax2.plot(errors)
    plt.show()


#Main
neurone=choixNeurone()
if neurone==1:
    f = ChooseAFunction1()
    if f==1:
        formelAffine()
    elif f==2:
        formelExpo()

elif neurone==2:
    tensor2(ChooseAFunction2())
